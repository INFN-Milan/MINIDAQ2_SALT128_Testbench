SOF

#################   MINIDAQ2 SALT128 TESTBENCH README  #################

                              INFN LHCb UT

          This project was created to log all the  activities
           regarding the SALT128 testbench in Milan. If you're 
           working  on  the  testbench, please keep in mind to 
           ALWAYS     UPDATE     THE      ACTIVITY    LOGBOOK.

########################################################################

                              USEFUL LINKS
                    
MINIDAQ Workshop @CERN:
https://indico.cern.ch/event/650060/

AMC40 documents and releases:
https://lbredmine.cern.ch/projects/amc40/documents

MiniDAQ HANDBOOK:
https://lbredmine.cern.ch/attachments/download/233/lhcb_upgrade_minidaq_handbook.pdf

GBTX MANUAL:
https://espace.cern.ch/GBT-Project/GBTX/Manuals/gbtxManual.pdf

VLDB PICTURES:
https://espace.cern.ch/GBT-Project/VLDB/Pictures%20and%20drawings/Forms/Thumbnails.aspx


########################################################################

EOF


